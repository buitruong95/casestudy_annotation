package JavaClasses;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "payments")
public class Payment extends Father implements Serializable{
    @EmbeddedId
    private PaymentId id;

    @Column(name = "checknumber")
    private String checkNumber;

    @Column(name = "amount")
    private double amount;

    @ManyToOne
    @JoinColumn(name = "customernumber", insertable = false, updatable = false)
    private Customer customer;

    public PaymentId getId() {
        return id;
    }

    public void setId(PaymentId id) {
        this.id = id;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
