package JavaClasses;

import javax.persistence.*;


@Entity
@Table(name = "productlines")
public class ProductLine extends Father{
    @Id
    @Column(name = "productline")
    private String productLine;

    @Column(name = "textdescription")
    private String textDescription;

    @Column(name = "htmldescription")
    private String htmlDescription;

    @Column(name = "image")
    private String image;

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    public String getHtmlDescription() {
        return htmlDescription;
    }

    public void setHtmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
